//+build wireinject

package wire

import (
	"github.com/google/wire"
	"gitlab.com/KostyaKulakov/moneydeal-database-service/internal/config"
	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/app/adapter"
	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/app/delivery/grpc/storage"
	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/app/infrastructure/repository"
	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/app/usecase"
)

//go:generate wire .

func InitializeService(cfg config.Config) (storage.Service, error) {
	wire.Build(
		storage.NewService,
		wire.Bind(new(storage.AccountUsecase), new(usecase.Account)),
		wire.Bind(new(storage.CategoryUsecase), new(usecase.Category)),
		wire.Bind(new(storage.GoalUsecase), new(usecase.Goal)),
		usecase.NewAccount,
		usecase.NewCategory,
		usecase.NewGoal,
		wire.Bind(new(usecase.AccountAdapter), new(adapter.Account)),
		wire.Bind(new(usecase.CategoryAdapter), new(adapter.Category)),
		wire.Bind(new(usecase.GoalAdapter), new(adapter.Goal)),
		adapter.NewAccount,
		adapter.NewCategory,
		adapter.NewGoal,
		wire.Bind(new(adapter.Repository), new(repository.Repository)),
		repository.New)
	return storage.Service{}, nil
}
