package main

import (
	"flag"
	"net"

	"github.com/sirupsen/logrus"
	"gitlab.com/KostyaKulakov/moneydeal-database-service/internal/config"
	"gitlab.com/KostyaKulakov/moneydeal-database-service/internal/wire"
	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/app/delivery/grpc/storage"
	"google.golang.org/grpc"
)

func main() {
	addr := flag.String("addr", "0.0.0.0:8082", "service address")
	databaseAddr := flag.String("dbAddr", "0.0.0.0", "database address")
	keyspace := flag.String("keyspace", "moneydeal", "database keyspace")
	flag.Parse()

	log := logrus.New()

	cfg := config.Config{
		DatabaseAddress: *databaseAddr,
		Keyspace:        *keyspace,
	}

	server := grpc.NewServer(grpc.UnaryInterceptor(storage.UnaryLogInterceptor(log)))

	storageServer, err := wire.InitializeService(cfg)
	if err != nil {
		log.Fatalf("failed to initilize storage server: %s", err)
	}

	storage.RegisterStorageServer(server, storageServer)

	lis, err := net.Listen("tcp", *addr)
	if err != nil {
		log.Fatal("failed to listen address", *addr)
	}

	log.Println(server.Serve(lis))
}
