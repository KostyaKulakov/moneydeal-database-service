module gitlab.com/KostyaKulakov/moneydeal-database-service

go 1.13

require (
	github.com/gocql/gocql v0.0.0-20191013011951-93ce931da9e1
	github.com/golang/protobuf v1.4.1
	github.com/google/wire v0.3.0
	github.com/mailru/easyjson v0.7.0
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/net v0.0.0-20191014212845-da9a3fd4c582 // indirect
	golang.org/x/sys v0.0.0-20191018095205-727590c5006e // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/genproto v0.0.0-20191009194640-548a555dbc03 // indirect
	google.golang.org/grpc v1.28.1
	google.golang.org/protobuf v1.22.0
)
