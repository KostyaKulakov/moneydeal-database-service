package errors

import "errors"

var (
	ErrNotFound    = errors.New("not found")
	ErrInvalidUUID = errors.New("invalid UUID")
)
