package storage

import (
	context "context"
	"time"

	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/app/delivery/grpc/key"

	"github.com/sirupsen/logrus"
	grpc "google.golang.org/grpc"
	status "google.golang.org/grpc/status"
)

// UnaryLogInterceptor returns a new unary server interceptors that adds logrus.Entry to the context
func UnaryLogInterceptor(l *logrus.Logger) grpc.UnaryServerInterceptor {
	return func(ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler) (interface{}, error) {
		log := l.WithField("method", info.FullMethod)
		ctx = context.WithValue(ctx, key.ContextLogger, log)
		start := time.Now()
		log.WithField("start", start).Info()

		resp, err := handler(ctx, req)

		end := time.Now()
		fields := logrus.Fields{
			"status": status.Code(err).String(),
			"end":    end,
			"time":   end.Sub(start),
		}
		log.WithFields(fields).Info()

		return resp, err
	}
}
