//go:generate protoc --go_out=plugins=grpc:. storage.proto

package storage

import (
	context "context"
	"time"

	"github.com/golang/protobuf/ptypes"
	"google.golang.org/grpc/codes"

	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/app/entity"
	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/errors"
	status "google.golang.org/grpc/status"
)

type AccountUsecase interface {
	Register(phone string) (string, error)
	GetIDByPhone(phone string) (string, error)
	UpdateInfo(acc *entity.Account) error
	BlockPhone(phone string) error
	UnblockPhone(phone string) error
	GetByPhone(phone string) (*entity.Account, error)
	GetByID(id string) (*entity.Account, error)
	GetExistPhone(phones []string) (*[]entity.AccountInfo, error)
}

type CategoryUsecase interface {
	Create(accountID string, category *entity.Category) error
	List(accountID string, isExpense bool, name string, limit uint64) (*[]entity.Category, error)
	GroupList(accountID string, name string, limit uint64) (*[]entity.GroupCategory, error)
	CreateGroup(accountID string, category *entity.GroupCategory) error
	AppendMembersGroup(accountID string, groupID string, name string, phones []string) error
	GroupMembersList(categoryID string, name string) (*[]entity.AccountInfo, error)
	CreatePayment(payment *entity.CategoryPayment) error
	ListPayments(accountID, categoryID string, from time.Time, limit uint64) (*[]entity.CategoryPayment, error)

	CreateGroupPayment(payment *entity.GroupCategoryPayment) error
	ListGroupPayments(categoryID string, from time.Time, limit uint64) (*[]entity.ResponseGroupCategoryPayment, error)
}

type GoalUsecase interface {
	Create(accountID string, goal *entity.Goal) error
	List(accountID string, from time.Time, limit uint64) (*[]entity.Goal, error)
	CreatePayment(payment *entity.GoalPayment) error
	ListPayments(accountID, goalID string, from time.Time, limit uint64) (*[]entity.GoalPayment, error)
}

// Service grpc service for working with moneydeal database
type Service struct {
	account  AccountUsecase
	category CategoryUsecase
	goal     GoalUsecase
}

// NewService create instance of StorageService
func NewService(account AccountUsecase, category CategoryUsecase, goal GoalUsecase) Service {
	return Service{
		account:  account,
		category: category,
		goal:     goal,
	}
}

// GetExistPhone return existed phones
func (s Service) GetExistPhone(ctx context.Context, request *PhonesInner) (*AccountResponse, error) {
	list, err := s.account.GetExistPhone(request.Phones)
	if err != nil {
		return nil, err
	}

	result := make([]*AccountInfo, 0, len(*list))
	for _, l := range *list {
		result = append(result, &AccountInfo{
			ID:        "",
			LastName:  l.LastName,
			FirstName: l.FirstName,
			Phone:     l.Phone,
			PhotoPath: l.PhotoPath,
		})
	}

	return &AccountResponse{
		Accounts: result,
	}, nil
}

// RegisterAccount create new account
func (s Service) RegisterAccount(ctx context.Context, request *Phone) (*AccountID, error) {
	id, err := s.account.Register(request.Phone)
	if err != nil {
		return &AccountID{}, err
	}

	return &AccountID{
		ID: id,
	}, nil
}

// GetAccountIDByPhone find account id by phone
func (s Service) GetAccountIDByPhone(ctx context.Context, request *Phone) (*AccountID, error) {
	id, err := s.account.GetIDByPhone(request.Phone)
	if err != nil {
		if err == errors.ErrNotFound {
			st := status.New(codes.NotFound, err.Error())
			return &AccountID{}, st.Err()
		}

		return &AccountID{}, err
	}

	return &AccountID{
		ID: id,
	}, nil
}

// UpdateAccountInfo update account info
func (s Service) UpdateAccountInfo(ctx context.Context, request *AccountInfo) (*Nothing, error) {
	info := &entity.Account{
		ID: request.ID,
		AccountInfo: entity.AccountInfo{
			FirstName: request.FirstName,
			LastName:  request.LastName,
			PhotoPath: request.PhotoPath,
		},
	}
	err := s.account.UpdateInfo(info)

	return &Nothing{}, err
}

// BlockPhone add phone to block list
func (s Service) BlockPhone(ctx context.Context, request *Phone) (*Nothing, error) {
	err := s.account.BlockPhone(request.Phone)
	return &Nothing{}, err
}

// UnblockPhone remove phone from block list
func (s Service) UnblockPhone(ctx context.Context, request *Phone) (*Nothing, error) {
	err := s.account.UnblockPhone(request.Phone)
	return &Nothing{}, err
}

// GetAccountByPhone find account by phone
func (s Service) GetAccountByPhone(ctx context.Context, request *Phone) (*AccountInfo, error) {
	acc, err := s.account.GetByPhone(request.Phone)
	if err != nil {
		if err == errors.ErrNotFound {
			st := status.New(codes.NotFound, err.Error())
			return &AccountInfo{}, st.Err()
		}

		return &AccountInfo{}, err
	}

	return &AccountInfo{
		ID:        acc.ID,
		FirstName: acc.FirstName,
		LastName:  acc.LastName,
		Phone:     acc.Phone,
		PhotoPath: acc.PhotoPath,
	}, nil
}

// GetAccountByID find account by id
func (s Service) GetAccountByID(ctx context.Context, request *AccountID) (*AccountInfo, error) {
	acc, err := s.account.GetByID(request.ID)
	if err != nil {
		if err == errors.ErrNotFound {
			st := status.New(codes.NotFound, err.Error())
			return &AccountInfo{}, st.Err()
		}

		return &AccountInfo{}, err
	}

	return &AccountInfo{
		ID:        acc.ID,
		FirstName: acc.FirstName,
		LastName:  acc.LastName,
		PhotoPath: acc.PhotoPath,
	}, nil
}

// CreateCategory add new category to account
func (s Service) CreateCategory(ctx context.Context, request *Category) (*Nothing, error) {
	category := &entity.Category{
		Name:      request.Name,
		IsExpense: request.IsExpense,
		ImagePath: request.ImagePath,
	}

	err := s.category.Create(request.AccountID, category)
	if err != nil {
		if err == errors.ErrNotFound {
			st := status.New(codes.NotFound, err.Error())
			return &Nothing{}, st.Err()
		}

		return &Nothing{}, err
	}

	return &Nothing{}, nil
}

func (s Service) CreateGroup(ctx context.Context, request *GroupCategory) (*Nothing, error) {
	category := &entity.GroupCategory{
		Name:        request.Name,
		ImagePath:   request.ImagePath,
		Description: request.Description,
	}

	err := s.category.CreateGroup(request.AccountID, category)
	if err != nil {
		if err == errors.ErrNotFound {
			st := status.New(codes.NotFound, err.Error())
			return &Nothing{}, st.Err()
		}

		return &Nothing{}, err
	}

	return &Nothing{}, nil
}

func (s Service) AppendMembersGroup(ctx context.Context, request *MembersGroup) (*Nothing, error) {
	err := s.category.AppendMembersGroup(request.AccountID, request.CategoryID, "", request.Phones)
	if err != nil {
		if err == errors.ErrNotFound {
			st := status.New(codes.NotFound, err.Error())
			return &Nothing{}, st.Err()
		}

		return &Nothing{}, err
	}

	return &Nothing{}, nil
}

// GroupMembersList return members of group
func (s Service) GroupMembersList(ctx context.Context, request *GroupMembersRequest) (*AccountList, error) {
	list, err := s.category.GroupMembersList(request.CategoryID, request.Name)
	if err != nil {
		if err == errors.ErrNotFound {
			st := status.New(codes.NotFound, err.Error())
			return &AccountList{}, st.Err()
		}

		return &AccountList{}, err
	}

	result := make([]*AccountInfo, 0, len(*list))
	for _, l := range *list {
		result = append(result, &AccountInfo{
			ID:        "",
			LastName:  l.LastName,
			FirstName: l.FirstName,
			Phone:     l.Phone,
			PhotoPath: l.PhotoPath,
		})
	}

	return &AccountList{
		List: result,
	}, nil
}

// GetCategoryList get list of categories for account
func (s Service) GetCategoryList(ctx context.Context, request *CategoryListRequest) (*CategoryList, error) {
	list, err := s.category.List(request.AccountID, request.IsExpense, request.Name, request.Limit)
	if err != nil {
		if err == errors.ErrNotFound {
			st := status.New(codes.NotFound, err.Error())
			return &CategoryList{}, st.Err()
		}

		return &CategoryList{}, err
	}

	result := make([]*Category, 0, len(*list))
	for _, l := range *list {
		lastUpdate, err := ptypes.TimestampProto(l.LastUpdate)
		if err != nil {
			return &CategoryList{}, err
		}

		result = append(result, &Category{
			ID:          l.ID,
			Name:        l.Name,
			Amount:      l.Amount,
			IsExpense:   l.IsExpense,
			LastUpdate:  lastUpdate,
			LastPayment: l.LastPayment,
			ImagePath:   l.ImagePath,
		})
	}

	return &CategoryList{
		List: result,
	}, nil
}

// GetGroupCategoryList get list of group categories for account
func (s Service) GetGroupCategoryList(ctx context.Context, request *GroupCategoryListRequest) (*GroupCategoryList, error) {
	list, err := s.category.GroupList(request.AccountID, request.Name, request.Limit)
	if err != nil {
		if err == errors.ErrNotFound {
			st := status.New(codes.NotFound, err.Error())
			return &GroupCategoryList{}, st.Err()
		}

		return &GroupCategoryList{}, err
	}

	result := make([]*GroupCategory, 0, len(*list))
	for _, l := range *list {
		lastUpdate, err := ptypes.TimestampProto(l.LastUpdate)
		if err != nil {
			return &GroupCategoryList{}, err
		}

		result = append(result, &GroupCategory{
			ID:          l.ID,
			Name:        l.Name,
			Amount:      l.Amount,
			Description: l.Description,
			LastUpdate:  lastUpdate,
			ImagePath:   l.ImagePath,
		})
	}

	return &GroupCategoryList{
		List: result,
	}, nil
}

func (s Service) CreateCategoryPayment(ctx context.Context, request *CategoryPayment) (*Nothing, error) {
	date, err := ptypes.Timestamp(request.Date)
	if err != nil {
		st := status.New(codes.InvalidArgument, err.Error())
		return &Nothing{}, st.Err()
	}

	payment := &entity.CategoryPayment{
		AccountID:   request.AccountID,
		Name:        request.Name,
		IsExpense:   request.IsExpense,
		Title:       request.Title,
		Description: request.Description,
		RecieptImg:  request.RecieptImg,
		Type:        request.Type,
		Amount:      request.Amount,
		Date:        date,
	}

	err = s.category.CreatePayment(payment)
	if err != nil {
		if err == errors.ErrNotFound {
			st := status.New(codes.NotFound, err.Error())
			return &Nothing{}, st.Err()
		}

		return &Nothing{}, err
	}

	return &Nothing{}, nil
}

func (s Service) GetCategoryPaymentList(ctx context.Context, request *CategoryPaymentListRequest) (*CategoryPaymentList, error) {
	from, err := ptypes.Timestamp(request.From)
	if err != nil {
		st := status.New(codes.InvalidArgument, err.Error())
		return &CategoryPaymentList{}, st.Err()
	}

	list, err := s.category.ListPayments(request.AccountID, request.CategoryID, from, request.Limit)
	if err != nil {
		if err == errors.ErrNotFound {
			st := status.New(codes.NotFound, err.Error())
			return &CategoryPaymentList{}, st.Err()
		}

		return &CategoryPaymentList{}, err
	}

	result := make([]*CategoryPaymentInfo, 0, len(*list))
	for _, l := range *list {
		date, err := ptypes.TimestampProto(l.Date)
		if err != nil {
			return &CategoryPaymentList{}, err
		}

		result = append(result, &CategoryPaymentInfo{
			Title:       l.Title,
			Description: l.Description,
			RecieptImg:  l.RecieptImg,
			Type:        l.Type,
			Date:        date,
			Amount:      l.Amount,
		})
	}

	return &CategoryPaymentList{
		List: result,
	}, nil
}

func (s Service) CreateGroupCategoryPayment(ctx context.Context, request *CategoryGroupPayment) (*Nothing, error) {
	date, err := ptypes.Timestamp(request.Date)
	if err != nil {
		st := status.New(codes.InvalidArgument, err.Error())
		return &Nothing{}, st.Err()
	}

	payment := &entity.GroupCategoryPayment{
		CategoryID:  request.CategoryID,
		AccountID:   request.AccountID,
		Name:        request.Name,
		Title:       request.Title,
		Description: request.Description,
		RecieptImg:  request.RecieptImg,
		Type:        request.Type,
		Amount:      request.Amount,
		Date:        date,
	}

	err = s.category.CreateGroupPayment(payment)
	if err != nil {
		if err == errors.ErrNotFound {
			st := status.New(codes.NotFound, err.Error())
			return &Nothing{}, st.Err()
		}

		return &Nothing{}, err
	}

	return &Nothing{}, nil
}

func (s Service) GetCategoryGroupPaymentList(ctx context.Context, request *CategoryGroupPaymentListRequest) (*CategoryGroupPaymentList, error) {
	from, err := ptypes.Timestamp(request.From)
	if err != nil {
		st := status.New(codes.InvalidArgument, err.Error())
		return &CategoryGroupPaymentList{}, st.Err()
	}

	list, err := s.category.ListGroupPayments(request.CategoryID, from, request.Limit)
	if err != nil {
		if err == errors.ErrNotFound {
			st := status.New(codes.NotFound, err.Error())
			return &CategoryGroupPaymentList{}, st.Err()
		}

		return &CategoryGroupPaymentList{}, err
	}

	result := make([]*CategoryGroupPaymentInfo, 0, len(*list))
	for _, l := range *list {
		date, err := ptypes.TimestampProto(l.Date)
		if err != nil {
			return &CategoryGroupPaymentList{}, err
		}

		result = append(result, &CategoryGroupPaymentInfo{
			Title:       l.Title,
			Description: l.Description,
			RecieptImg:  l.RecieptImg,
			Type:        l.Type,
			Date:        date,
			Amount:      l.Amount,
			Owner: &AccountInfo{
				ID:        "",
				LastName:  l.Owner.LastName,
				FirstName: l.Owner.FirstName,
				Phone:     l.Owner.Phone,
				PhotoPath: l.Owner.PhotoPath,
			},
		})
	}

	return &CategoryGroupPaymentList{
		List: result,
	}, nil
}

func (s Service) CreateGoal(ctx context.Context, request *Goal) (*Nothing, error) {
	finish, err := ptypes.Timestamp(request.Finish)
	if err != nil {
		st := status.New(codes.InvalidArgument, err.Error())
		return &Nothing{}, st.Err()
	}

	goal := &entity.Goal{
		Title:   request.Title,
		Balance: request.Balance,
		Amount:  request.Amount,
		Finish:  finish,
		Photo:   request.Photo,
	}

	err = s.goal.Create(request.AccountID, goal)
	if err != nil {
		if err == errors.ErrNotFound {
			st := status.New(codes.NotFound, err.Error())
			return &Nothing{}, st.Err()
		}

		return &Nothing{}, err
	}

	return &Nothing{}, nil
}

func (s Service) GetGoalList(ctx context.Context, request *GoalListRequest) (*GoalList, error) {
	from, err := ptypes.Timestamp(request.From)
	if err != nil {
		st := status.New(codes.InvalidArgument, err.Error())
		return &GoalList{}, st.Err()
	}

	list, err := s.goal.List(request.ID, from, request.Limit)
	if err != nil {
		if err == errors.ErrNotFound {
			st := status.New(codes.NotFound, err.Error())
			return &GoalList{}, st.Err()
		}

		return &GoalList{}, err
	}

	result := make([]*Goal, 0, len(*list))
	for _, l := range *list {
		finish, err := ptypes.TimestampProto(l.Finish)
		if err != nil {
			return &GoalList{}, err
		}

		result = append(result, &Goal{
			ID:      l.ID,
			Title:   l.Title,
			Amount:  l.Amount,
			Balance: l.Balance,
			Finish:  finish,
			Photo:   l.Photo,
		})
	}

	return &GoalList{
		List: result,
	}, nil
}

func (s Service) CreateGoalPayment(ctx context.Context, request *GoalPayment) (*Nothing, error) {
	date, err := ptypes.Timestamp(request.Date)
	if err != nil {
		st := status.New(codes.InvalidArgument, err.Error())
		return &Nothing{}, st.Err()
	}

	finish, err := ptypes.Timestamp(request.GoalFinish)
	if err != nil {
		st := status.New(codes.InvalidArgument, err.Error())
		return &Nothing{}, st.Err()
	}

	payment := &entity.GoalPayment{
		GoalID:     request.GoalID,
		AccountID:  request.AccountID,
		Title:      request.Title,
		Amount:     request.Amount,
		GoalFinish: finish,
		Date:       date,
	}

	err = s.goal.CreatePayment(payment)
	if err != nil {
		if err == errors.ErrNotFound {
			st := status.New(codes.NotFound, err.Error())
			return &Nothing{}, st.Err()
		}

		return &Nothing{}, err
	}

	return &Nothing{}, nil
}

func (s Service) GetGoalPaymentList(ctx context.Context, request *GoalPaymentListRequest) (*GoalPaymentList, error) {
	from, err := ptypes.Timestamp(request.From)
	if err != nil {
		st := status.New(codes.InvalidArgument, err.Error())
		return &GoalPaymentList{}, st.Err()
	}

	list, err := s.goal.ListPayments(request.AccountID, request.GoalID, from, request.Limit)
	if err != nil {
		if err == errors.ErrNotFound {
			st := status.New(codes.NotFound, err.Error())
			return &GoalPaymentList{}, st.Err()
		}

		return &GoalPaymentList{}, err
	}

	result := make([]*GoalPaymentInfo, 0, len(*list))
	for _, l := range *list {
		date, err := ptypes.TimestampProto(l.Date)
		if err != nil {
			return &GoalPaymentList{}, err
		}

		result = append(result, &GoalPaymentInfo{
			Title:  l.Title,
			Date:   date,
			Amount: l.Amount,
		})
	}

	return &GoalPaymentList{
		List: result,
	}, nil
}
