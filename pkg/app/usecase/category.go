package usecase

import (
	"time"

	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/app/entity"
)

type CategoryAdapter interface {
	Create(accountID string, category *entity.Category) error
	List(accountID string, isExpense bool, name string, limit uint64) (*[]entity.Category, error)
	GroupList(accountID string, name string, limit uint64) (*[]entity.GroupCategory, error)
	CreatePayment(payment *entity.CategoryPayment) error
	ListPayments(accountID, categoryID string, from time.Time, limit uint64) (*[]entity.CategoryPayment, error)
	CreateGroup(accountID string, category *entity.GroupCategory) error
	AppendMembersGroup(accountID string, groupID string, name string, phones []string) error
	GroupMembersList(categoryID string, name string) (*[]entity.AccountInfo, error)

	CreateGroupPayment(payment *entity.GroupCategoryPayment) error
	ListGroupPayments(categoryID string, from time.Time, limit uint64) (*[]entity.ResponseGroupCategoryPayment, error)
}

// Category perform category use cases
type Category struct {
	adapter CategoryAdapter
}

// NewCategory create instance of Category
func NewCategory(adapter CategoryAdapter) Category {
	return Category{
		adapter: adapter,
	}
}

// Create add new category to account
func (c Category) Create(accountID string, category *entity.Category) error {
	return c.adapter.Create(accountID, category)
}

// CreateGroup add new group category to account
func (c Category) CreateGroup(accountID string, category *entity.GroupCategory) error {
	return c.adapter.CreateGroup(accountID, category)
}

// AppendMembersGroup add new members to group
func (c Category) AppendMembersGroup(accountID string, groupID string, name string, phones []string) error {
	return c.adapter.AppendMembersGroup(accountID, groupID, "", phones)
}

// GroupMembersList list of members to group
func (c Category) GroupMembersList(categoryID string, name string) (*[]entity.AccountInfo, error) {
	return c.adapter.GroupMembersList(categoryID, name)
}

// List get list of categories
func (c Category) List(accountID string, isExpense bool, name string, limit uint64) (*[]entity.Category, error) {
	return c.adapter.List(accountID, isExpense, name, limit)
}

// GroupList get list of categories
func (c Category) GroupList(accountID string, name string, limit uint64) (*[]entity.GroupCategory, error) {
	return c.adapter.GroupList(accountID, name, limit)
}

func (c Category) CreatePayment(payment *entity.CategoryPayment) error {
	return c.adapter.CreatePayment(payment)
}

func (c Category) ListPayments(accountID, categoryID string, from time.Time, limit uint64) (*[]entity.CategoryPayment, error) {
	return c.adapter.ListPayments(accountID, categoryID, from, limit)
}

func (c Category) CreateGroupPayment(payment *entity.GroupCategoryPayment) error {
	return c.adapter.CreateGroupPayment(payment)
}

func (c Category) ListGroupPayments(categoryID string, from time.Time, limit uint64) (*[]entity.ResponseGroupCategoryPayment, error) {
	return c.adapter.ListGroupPayments(categoryID, from, limit)
}
