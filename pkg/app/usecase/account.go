package usecase

import (
	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/app/entity"
	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/errors"
)

type AccountAdapter interface {
	GetIDByPhone(phone string) (string, error)
	GetByID(id string) (*entity.Account, error)
	Update(acc *entity.Account) error
	Create(id, phone string) error
	UpdateStatus(phone string, status bool) error
	CreatePhone(phone string) (string, error)
}

// Account perform account use cases
type Account struct {
	adapter AccountAdapter
}

// GetExistPhone get list of existed accounts
func (a Account) GetExistPhone(phones []string) (*[]entity.AccountInfo, error) {
	result := make([]entity.AccountInfo, 0, len(phones))

	for _, l := range phones {

		phoneInfo, err := a.GetByPhone(l)
		if err != nil {
			continue
		}

		result = append(result, phoneInfo.AccountInfo)
	}

	return &result, nil
}

// NewAccount create instance of AccountUsecase
func NewAccount(adapter AccountAdapter) Account {
	return Account{
		adapter: adapter,
	}
}

// Register create account id for phone
func (a Account) Register(phone string) (string, error) {
	id, err := a.adapter.CreatePhone(phone)
	if err != nil {
		return "", err
	}

	if err := a.adapter.Create(id, phone); err != nil {
		return "", err
	}

	return id, nil
}

// GetIDByPhone find account id by phone
func (a Account) GetIDByPhone(phone string) (string, error) {
	id, err := a.adapter.GetIDByPhone(phone)
	if err != nil {
		return "", err
	}

	return id, nil
}

// UpdateInfo update account info
func (a Account) UpdateInfo(acc *entity.Account) error {
	if err := a.adapter.Update(acc); err != nil {
		return err
	}

	return nil
}

// BlockPhone add phone to block list
func (a Account) BlockPhone(phone string) error {
	return a.adapter.UpdateStatus(phone, false)
}

// UnblockPhone remove phone from block list
func (a Account) UnblockPhone(phone string) error {
	return a.adapter.UpdateStatus(phone, true)
}

// GetByPhone find account by phone
func (a Account) GetByPhone(phone string) (*entity.Account, error) {
	id, err := a.adapter.GetIDByPhone(phone)
	if err != nil {
		return nil, err
	}

	acc, err := a.adapter.GetByID(id)
	if err == errors.ErrNotFound {
		if err := a.adapter.Create(id, phone); err != nil {
			return nil, err
		}

		return acc, nil
	}

	if err != nil {
		return nil, err
	}

	return acc, nil
}

// GetByID find account by id
func (a Account) GetByID(id string) (*entity.Account, error) {
	return a.adapter.GetByID(id)
}
