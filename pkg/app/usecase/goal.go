package usecase

import (
	"time"

	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/app/entity"
)

type GoalAdapter interface {
	Create(accountID string, goal *entity.Goal) error
	List(accountID string, from time.Time, limit uint64) (*[]entity.Goal, error)
	CreatePayment(payment *entity.GoalPayment) error
	ListPayments(accountID, goalID string, from time.Time, limit uint64) (*[]entity.GoalPayment, error)
}

// Goal perform goal use cases
type Goal struct {
	adapter GoalAdapter
}

// NewCategory create instance of Category
func NewGoal(adapter GoalAdapter) Goal {
	return Goal{
		adapter: adapter,
	}
}

func (g Goal) Create(accountID string, goal *entity.Goal) error {
	return g.adapter.Create(accountID, goal)
}

func (g Goal) List(accountID string, from time.Time, limit uint64) (*[]entity.Goal, error) {
	return g.adapter.List(accountID, from, limit)
}

func (g Goal) CreatePayment(payment *entity.GoalPayment) error {
	return g.adapter.CreatePayment(payment)
}

func (g Goal) ListPayments(accountID, goalID string, from time.Time, limit uint64) (*[]entity.GoalPayment, error) {
	return g.adapter.ListPayments(accountID, goalID, from, limit)
}
