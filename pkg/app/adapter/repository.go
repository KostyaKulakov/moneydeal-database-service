package adapter

type Repository interface {
	Get(request string, result []interface{}, params ...interface{}) error
	Insert(request string, params ...interface{}) error
	Update(request string, params ...interface{}) error
	Select(request string, params ...interface{}) []map[string]interface{}
}
