package adapter

import (
	"time"

	"github.com/gocql/gocql"
	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/app/entity"
	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/app/infrastructure/repository"
	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/errors"
)

type Goal struct {
	repo Repository
}

func NewGoal(repo Repository) Goal {
	return Goal{
		repo: repo,
	}
}

func (g Goal) Create(accountID string, goal *entity.Goal) error {
	accountUUID, err := gocql.ParseUUID(accountID)
	if err != nil {
		return errors.ErrInvalidUUID
	}

	goalUUID := gocql.TimeUUID()
	goal.ID = goalUUID.String()

	return g.repo.Insert(repository.QueryGoalInsert, goalUUID, accountUUID, goal.Title, goal.Amount, goal.Balance, goal.Finish, goal.Photo)
}

func (g Goal) ListPayments(accountID, goalID string, from time.Time, limit uint64) (*[]entity.GoalPayment, error) {
	accountUUID, err := gocql.ParseUUID(accountID)
	if err != nil {
		return nil, errors.ErrInvalidUUID
	}

	goalUUID, err := gocql.ParseUUID(goalID)
	if err != nil {
		return nil, errors.ErrInvalidUUID
	}

	list := g.repo.Select(repository.QueryGoalPayment, accountUUID, goalUUID, from, limit)

	payments := make([]entity.GoalPayment, 0, len(list))
	for _, l := range list {
		payments = append(payments, entity.GoalPayment{
			Title:  l["title"].(string),
			Amount: l["amount"].(int64),
			Date:   l["date"].(time.Time),
		})
	}

	return &payments, nil
}

func (g Goal) List(accountID string, from time.Time, limit uint64) (*[]entity.Goal, error) {
	accountUUID, err := gocql.ParseUUID(accountID)
	if err != nil {
		return nil, errors.ErrInvalidUUID
	}

	list := g.repo.Select(repository.QueryGoalByAccountID, accountUUID, from, limit)

	goals := make([]entity.Goal, 0, len(list))
	for _, l := range list {
		goals = append(goals, entity.Goal{
			ID:      l["id"].(gocql.UUID).String(),
			Title:   l["title"].(string),
			Balance: l["balance"].(int64),
			Amount:  l["amount"].(int64),
			Finish:  l["finish"].(time.Time),
			Photo:   l["photo"].(string),
		})
	}

	return &goals, nil
}

func (g Goal) CreatePayment(payment *entity.GoalPayment) error {
	accountUUID, err := gocql.ParseUUID(payment.AccountID)
	if err != nil {
		return errors.ErrInvalidUUID
	}

	goalUUID, err := gocql.ParseUUID(payment.GoalID)
	if err != nil {
		return errors.ErrInvalidUUID
	}

	goal := g.repo.Select(repository.QueryGoalBalance, accountUUID, payment.GoalFinish, goalUUID)
	if len(goal) != 1 {
		return errors.ErrNotFound
	}

	newBalance := goal[0]["balance"].(int64) + payment.Amount

	if err := g.repo.Update(repository.QueryGoalBalanceUpdate, newBalance, accountUUID, payment.GoalFinish, goalUUID); err != nil {
		return err
	}

	if err := g.repo.Insert(repository.QueryGoalPaymentInsert, goalUUID, accountUUID, payment.Title, payment.Amount, payment.Date); err != nil {
		return err
	}

	return nil
}
