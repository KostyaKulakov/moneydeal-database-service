package adapter

import (
	"github.com/gocql/gocql"
	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/app/entity"
	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/app/infrastructure/repository"
	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/errors"
)

// Account perform queries to repository
type Account struct {
	repo Repository
}

// NewAccount create new instance of account
func NewAccount(repo Repository) Account {
	return Account{
		repo: repo,
	}
}

// GetIDByPhone receive account id by phone number
func (a Account) GetIDByPhone(phone string) (string, error) {
	var account entity.Account
	err := a.repo.Get(repository.QueryAccountIDByPhone, []interface{}{&account.Phone, &account.ID, &account.Status}, phone)

	switch err {
	case nil:
		return account.ID, nil
	case gocql.ErrNotFound:
		return "", errors.ErrNotFound
	default:
		return "", err
	}
}

// GetByID receive account info by id number
func (a Account) GetByID(id string) (*entity.Account, error) {
	var account entity.Account

	uuid, err := gocql.ParseUUID(id)
	if err != nil {
		return nil, errors.ErrInvalidUUID
	}

	res := []interface{}{&account.ID, &account.FirstName, &account.LastName, &account.Phone, &account.PhotoPath}

	if err := a.repo.Get(repository.QueryAccountByID, res, uuid); err != nil {
		if err == gocql.ErrNotFound {
			return nil, errors.ErrNotFound
		}

		return nil, err
	}

	return &account, nil
}

// Update renew account info
func (a Account) Update(acc *entity.Account) error {
	uuid, err := gocql.ParseUUID(acc.ID)
	if err != nil {
		return errors.ErrInvalidUUID
	}

	if err := a.repo.Insert(repository.QueryAccountUpdate, uuid, acc.FirstName, acc.LastName, acc.PhotoPath); err != nil {
		return err
	}

	return nil
}

// Create add new account
func (a Account) Create(id, phone string) error {
	uuid, err := gocql.ParseUUID(id)
	if err != nil {
		return errors.ErrInvalidUUID
	}

	if err := a.repo.Insert(repository.QueryAccountInsert, uuid, phone); err != nil {
		return err
	}

	return nil
}

// UpdateStatus update account status
func (a Account) UpdateStatus(phone string, status bool) error {
	return a.repo.Update(repository.QueryAccountUpdateStatus, status, phone)
}

// CreatePhone add new account phone
func (a Account) CreatePhone(phone string) (string, error) {
	uuid := gocql.TimeUUID()
	if err := a.repo.Insert(repository.QueryAccountInsertPhone, phone, uuid, true); err != nil {
		return "", err
	}

	return uuid.String(), nil
}
