package adapter

import (
	"strings"
	"time"

	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/app/entity"

	"github.com/gocql/gocql"
	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/app/infrastructure/repository"
	"gitlab.com/KostyaKulakov/moneydeal-database-service/pkg/errors"
)

type Category struct {
	repo Repository
}

func NewCategory(repo Repository) Category {
	return Category{
		repo: repo,
	}
}

// Create add new category to account
func (c Category) Create(accountID string, category *entity.Category) error {
	accountUUID, err := gocql.ParseUUID(accountID)
	if err != nil {
		return errors.ErrInvalidUUID
	}

	categoryUUID := gocql.TimeUUID()
	category.ID = categoryUUID.String()

	return c.repo.Insert(repository.QueryCategoryInsert, categoryUUID, accountUUID, strings.ToLower(category.Name),
		0, time.Now(), category.IsExpense, category.ImagePath, 0)
}

func (c Category) CreateGroup(accountID string, category *entity.GroupCategory) error {
	accountUUID, err := gocql.ParseUUID(accountID)
	if err != nil {
		return errors.ErrInvalidUUID
	}

	categoryUUID := gocql.TimeUUID()
	category.ID = categoryUUID.String()

	c.repo.Insert(repository.QueryGroupAppendOwnersInsert, categoryUUID, accountUUID, strings.ToLower(category.Name))
	c.repo.Insert(repository.QueryGroupAppendMemberInsert, categoryUUID, accountUUID, strings.ToLower(category.Name))

	return c.repo.Insert(repository.QueryGroupCategoryInsert, categoryUUID, accountUUID, strings.ToLower(category.Name),
		0, category.Description, time.Now(), category.ImagePath)
}

func (c Category) AppendMembersGroup(accountID string, groupID string, name string, phones []string) error {
	// accountUUID, err := gocql.ParseUUID(accountID)
	// if err != nil {
	// 	return errors.ErrInvalidUUID
	// }

	categoryUUID, err := gocql.ParseUUID(groupID)
	if err != nil {
		return errors.ErrInvalidUUID
	}

	var groupData entity.GroupCategory
	res := []interface{}{&groupData.ID, &groupData.Name, &groupData.AccountID, &groupData.Amount, &groupData.Description, &groupData.ImagePath, &groupData.LastUpdate}

	if err := c.repo.Get(repository.QueryGroupCategoryByID, res, categoryUUID); err != nil {
		if err == gocql.ErrNotFound {
			return errors.ErrNotFound
		}

		return err
	}

	for _, l := range phones {
		var account entity.Account
		err := c.repo.Get(repository.QueryAccountIDByPhone, []interface{}{&account.Phone, &account.ID, &account.Status}, l)

		if err != nil {
			continue
		}

		c.repo.Insert(repository.QueryGroupAppendMemberInsert, categoryUUID, account.ID, groupData.Name)
		c.repo.Insert(repository.QueryGroupAppendOwnersInsert, categoryUUID, account.ID, groupData.Name)
	}

	return nil
}

// GroupList get list of account group categories
func (c Category) GroupMembersList(categoryID string, name string) (*[]entity.AccountInfo, error) {
	categoryUUID, err := gocql.ParseUUID(categoryID)
	if err != nil {
		return nil, errors.ErrInvalidUUID
	}

	list := c.repo.Select(repository.QueryGroupMembersByCategoryID, categoryUUID)

	accounts := make([]entity.AccountInfo, 0, len(list))
	for _, l := range list {
		accountUUID := l["account_id"].(gocql.UUID)

		var account entity.Account
		res := []interface{}{&account.ID, &account.FirstName, &account.LastName, &account.Phone, &account.PhotoPath}

		if err := c.repo.Get(repository.QueryAccountByID, res, accountUUID); err != nil {
			if err == gocql.ErrNotFound {
				continue
			}

			continue
		}

		accounts = append(accounts, account.AccountInfo)
	}

	return &accounts, nil
}

// GroupList get list of account group categories
func (c Category) GroupList(accountID string, name string, limit uint64) (*[]entity.GroupCategory, error) {
	listCategoryID, err := c.GroupListID(accountID, name, limit)

	if err != nil {
		return nil, errors.ErrNotFound
	}

	categories := make([]entity.GroupCategory, 0, len(listCategoryID))

	for _, l := range listCategoryID {
		category, err := c.GroupByUUID(l)

		if err != nil {
			continue
		}

		categories = append(categories, (*category))
	}

	return &categories, nil
}

// GroupListID get list of account group categories
func (c Category) GroupListID(accountID string, name string, limit uint64) ([]gocql.UUID, error) {
	accountUUID, err := gocql.ParseUUID(accountID)
	if err != nil {
		return nil, errors.ErrInvalidUUID
	}

	list := c.repo.Select(repository.QueryGroupCategoryByAccountID, accountUUID, name, limit)

	categoriesID := make([]gocql.UUID, 0, len(list))
	for _, l := range list {
		categoriesID = append(categoriesID, l["category_id"].(gocql.UUID))
	}

	return categoriesID, nil
}

// GroupByUUID get list of account categories
func (c Category) GroupByUUID(groupID gocql.UUID) (*entity.GroupCategory, error) {
	list := c.repo.Select(repository.QueryGroupCategoryByID, groupID)

	if len(list) == 0 {
		return nil, errors.ErrInvalidUUID
	}

	l := list[0]

	return &entity.GroupCategory{
		ID:          l["id"].(gocql.UUID).String(),
		Name:        l["name"].(string),
		Amount:      l["amount"].(int64),
		Description: l["description"].(string),
		LastUpdate:  l["last_update"].(time.Time),
		ImagePath:   l["image_path"].(string),
	}, nil

}

// List get list of account categories
func (c Category) List(accountID string, isExpense bool, name string, limit uint64) (*[]entity.Category, error) {
	accountUUID, err := gocql.ParseUUID(accountID)
	if err != nil {
		return nil, errors.ErrInvalidUUID
	}

	list := c.repo.Select(repository.QueryCategoryByAccountID, accountUUID, isExpense, name, limit)

	categories := make([]entity.Category, 0, len(list))
	for _, l := range list {
		categories = append(categories, entity.Category{
			ID:          l["id"].(gocql.UUID).String(),
			Name:        l["name"].(string),
			Amount:      l["amount"].(int64),
			IsExpense:   l["is_expense"].(bool),
			LastUpdate:  l["last_update"].(time.Time),
			LastPayment: l["last_payment"].(int64),
			ImagePath:   l["image_path"].(string),
		})
	}

	return &categories, nil
}

func (c Category) CreateGroupPayment(payment *entity.GroupCategoryPayment) error {
	accountUUID, err := gocql.ParseUUID(payment.AccountID)
	if err != nil {
		return errors.ErrInvalidUUID
	}

	categoryUUID, err := gocql.ParseUUID(payment.CategoryID)
	if err != nil {
		return errors.ErrInvalidUUID
	}

	category := c.repo.Select(repository.QueryGroupCategoryByID, categoryUUID)
	if len(category) != 1 {
		return errors.ErrNotFound
	}

	newAmount := category[0]["amount"].(int64) + payment.Amount

	if err := c.repo.Update(repository.QueryGroupCategoryUpdate, newAmount, time.Now(), categoryUUID, payment.Name); err != nil {
		return err
	}

	if err := c.repo.Insert(repository.QueryGroupCategoryPaymentInsert, categoryUUID, accountUUID, payment.Title, payment.Description, payment.RecieptImg, payment.Type, payment.Amount, payment.Date); err != nil {
		return err
	}

	return nil
}

func (c Category) CreatePayment(payment *entity.CategoryPayment) error {
	accountUUID, err := gocql.ParseUUID(payment.AccountID)
	if err != nil {
		return errors.ErrInvalidUUID
	}

	category := c.repo.Select(repository.QueryCategory, accountUUID, payment.IsExpense, payment.Name)
	if len(category) != 1 {
		return errors.ErrNotFound
	}

	newAmount := category[0]["amount"].(int64) + payment.Amount
	categoryUUID := category[0]["id"].(gocql.UUID).String()

	if err := c.repo.Update(repository.QueryCategoryUpdate, newAmount, payment.Amount, time.Now(), accountUUID, payment.IsExpense, payment.Name); err != nil {
		return err
	}

	if err := c.repo.Insert(repository.QueryCategoryPaymentInsert, categoryUUID, accountUUID, payment.Title, payment.Description, payment.RecieptImg, payment.Type, payment.Amount, payment.Date); err != nil {
		return err
	}

	return nil
}

func (c Category) ListPayments(accountID, categoryID string, from time.Time, limit uint64) (*[]entity.CategoryPayment, error) {
	accountUUID, err := gocql.ParseUUID(accountID)
	if err != nil {
		return nil, errors.ErrInvalidUUID
	}

	categoryUUID, err := gocql.ParseUUID(categoryID)
	if err != nil {
		return nil, errors.ErrInvalidUUID
	}

	list := c.repo.Select(repository.QueryCategoryPayment, accountUUID, categoryUUID, from, limit)

	payments := make([]entity.CategoryPayment, 0, len(list))
	for _, l := range list {
		payments = append(payments, entity.CategoryPayment{
			Title:       l["title"].(string),
			Description: l["description"].(string),
			RecieptImg:  l["reciept_image"].(string),
			Type:        l["type"].(string),
			Amount:      l["amount"].(int64),
			Date:        l["date"].(time.Time),
		})
	}

	return &payments, nil
}

func (c Category) ListGroupPayments(categoryID string, from time.Time, limit uint64) (*[]entity.ResponseGroupCategoryPayment, error) {
	categoryUUID, err := gocql.ParseUUID(categoryID)
	if err != nil {
		return nil, errors.ErrInvalidUUID
	}

	list := c.repo.Select(repository.QueryGroupCategoryPayment, categoryUUID, from, limit)

	payments := make([]entity.ResponseGroupCategoryPayment, 0, len(list))
	for _, l := range list {
		var account entity.Account
		res := []interface{}{&account.ID, &account.FirstName, &account.LastName, &account.Phone, &account.PhotoPath}

		if err := c.repo.Get(repository.QueryAccountByID, res, l["account_id"].(gocql.UUID)); err != nil {
			if err == gocql.ErrNotFound {
				continue
			}

			continue
		}

		payments = append(payments, entity.ResponseGroupCategoryPayment{
			Title:       l["title"].(string),
			Description: l["description"].(string),
			RecieptImg:  l["reciept_image"].(string),
			Type:        l["type"].(string),
			Amount:      l["amount"].(int64),
			Owner:       account.AccountInfo,
			Date:        l["date"].(time.Time),
		})
	}

	return &payments, nil
}
