package entity

import "time"

//go:generate easyjson category.go

// Category store information about account
type Category struct {
	ID          string
	AccountID   string
	Name        string
	Amount      int64
	IsExpense   bool
	LastUpdate  time.Time
	LastPayment int64
	ImagePath   string
}

// CategoryPayment store payment associated with category
type CategoryPayment struct {
	AccountID   string
	Name        string
	IsExpense   bool
	Title       string
	Description string
	RecieptImg  string
	Type        string
	Amount      int64
	Date        time.Time
}

// ResponseGroupCategoryPayment store payment associated with category
type ResponseGroupCategoryPayment struct {
	CategoryID  string
	Name        string
	Title       string
	Description string
	RecieptImg  string
	Type        string
	Amount      int64
	Owner       AccountInfo
	Date        time.Time
}

// GroupCategoryPayment store payment associated with category
type GroupCategoryPayment struct {
	AccountID   string
	CategoryID  string
	Name        string
	Title       string
	Description string
	RecieptImg  string
	Type        string
	Amount      int64
	Date        time.Time
}

// GroupCategory store information about group category
type GroupCategory struct {
	ID          string
	AccountID   string
	Name        string
	Amount      int64
	Description string
	LastUpdate  time.Time
	ImagePath   string
}
